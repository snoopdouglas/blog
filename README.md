# 2020-09-20 - It costs nothing to unsubscribe

Wassup my party rockers, it's time for another blog. This is the first since I "started" in April (shut up). Fortunately, I've been blessed with plenty to keep me busy since then, so can you blame me? Yes, you can.

[![Fatalistic Link](/archive/_images/fatalistic-link.png)](https://shakoribe.tumblr.com/post/159224759661)<br>
<small>*Guess I'll eat a three-course meal.*</small>

Those of you who know me from university (or maybe a bit earlier) won't be surprised by this at all, but the event that's brought me out of my immediate hiatus is that **Pendulum** have released some new music.

Being a pussyboi in my teens, I wasn't a fan of proper drum & bass. Even [Slam](https://www.youtube.com/watch?v=gIOQfdn9L9c) was a bit much for me, though I had a lot of time for the [Voodoo People remix](https://www.youtube.com/watch?v=XQEBzauVIlA). So when they went all kinda pop-rock-electronica, I was conflicted (read: couldn't help liking their new stuff).

![Immersion crop](/archive/_images/immersion-crop.jpg)<br>
<small>*All about that bass*</small>

So, here we are a decade after Immersion. Rob and Gareth have put their <abbr title="You liked Knife Party at the time.">gross disgusting side project of which we must not speak</abbr> aside, and we've got new music.

---

## Stadium drum & bass in the mumble rap age

There are two new tracks: **Driver** and **Nothing For Free**.

Listening to the reveal of the latter on Radio 1 was a bit weird. Hype amongst the listeners seemed pretty real, though while I can't be sure what Annie Mac actually made of it, she didn't sound gassed.

Let's go off on a quick speculative tangent here. Annie's a top DJ (both in radio and mix), but nobody can avoid being a product of their time. In 2010, Zane Lowe had her job; Annie had a later timeslot, as I recall. Both of them gushed about this band.

So, part of me wonders whether this play was oddly saddening for her. For all of their pretence and ridiculousness, modern 'rock band' Pendulum has always been *fun*. You're not encouraged to think too hard when listening; you're totally cool to just let those drops lower your IQ by 50 points and ride out the vocals on hype. Sound familiar?

Ah, but you don't see Rob gawping at you from a jet ski. Not our Rob - he's busy brooding about some nonspecific epicness in a darkened room, a single downlight illuminating him and his bandmates - and yet as they stand there side by side, all looking at you, yes *you* - you can't help but wonder how much is, er, actually going on in there.

![Pendulum in a darkened room](/archive/_images/pendulum.jpg)<br>
<small>*Buckle up, kid. We got stories to tell. Stories about stuff that happens.*</small>

This is epic electronica, but without much of an agenda. Pop music is often polarised between pigshit thickness and a smack over the head with the wokehammer. Pendulum isn't really either, which may be linked with Rob publicly being an extremely grumpy old man.

So, as Annie played this track and heard him belting it out about, well, something, I have to wonder whether she found herself feeling sad - or perhaps confounded - at premiering what pop music "used to sound like". Pendulum were the future once…

## [Driver](https://www.youtube.com/watch?v=O3FKMYBV01U)

Now, let me almost immediately contradict myself.

[![Driver cover art](/archive/_images/pendulum-driver.jpg)](https://www.youtube.com/watch?v=O3FKMYBV01U)

This is solid. Powerful but unobnoxious leads give way to growling, grinding, brilliantly textured bass of yore. Perfectly-tuned drums, and the tiny B section goes to the jungle. It even has a hilariously daft knod to the end of Immersion (that notably doesn't get in the way).

Best of all: being proper drum & bass, there are no vocals! It's a lot more difficult to fall into the "epic without cause" trap when this is the case. Good jarb lads.

Now while I do have a nagging suspicion (read: know full well) that I'm enjoying this so much because *it's Pendulum*, there are a few of us who've been wanting this track to appear since about 2005 - and while this doesn't apply so much to me, knowing that makes it all the sweeter. Immersion gave us [Salt in the Wounds](https://www.youtube.com/watch?v=l1T74w-Cc6I), which was, eh, basically Slam. This, on the other hand, is in a different key, and has a different structure!

![Lawrence off of Ratchet and Clank 3 and that](/archive/_images/lawrence.jpg)<br>
<small>*Really, sir? What a treat.*</small>

With all that in mind, things are looking rosy for the tru blu dnb kru. You know how it is, though: for every Pendulum banger, there exists an equal and opposite melodic stadium pop track.

## [Nothing For Free]((https://www.youtube.com/watch?v=WeOFimyghIA))

[![It's broodin' time](/archive/_images/pendulum-nothing-for-free.jpg)](https://www.youtube.com/watch?v=WeOFimyghIA)<br>
<small>*One hundred Australian dollars for anyone who can find me a picture of this man not wearing black*</small>

Imagine my surprise when [UKF *Dubstep*](https://www.youtube.com/watch?v=LacUWoDycT8) dropped this. Real sign of the times there: when Pendulum last released music, UKF's dubstep channel was *way* bigger than their drum & bass one. Hilariously, the former has actually retained its subscribers over the latter, even though its views are now halved - but I digress.

I'm going to make like it's 2012 and say that this is actually drumstep. It's like [Watercolour](https://www.youtube.com/watch?v=tEPB7uzKuh4) versus [something Nero would make](https://www.youtube.com/watch?v=Nyz4h18FH1g). It's good, it's grown on me, but the drop is a bit shit unless you're absolutely shafting your eardrums. This is very firmly 2010-esque Pendulum, and as such, I'd say it's good pop music; it takes me straight back to university, stupid decisions, heartbreak and quadvods.

### Subject matter

If you clicked above and/or watched the video, let's talk about that and the lyrics for a bit. Rob mentioned that they've been working on new music since early this year; there wasn't much cause for writing songs about zoonotic-origin pathogens back then, but he did also mention that they've "tweaked things" since.

My diagnosis here is therefore that this song was written when we were concentrating on World War 3 at the beginning of the year. Do your millenial goldfish brains remember this? Things were getting a bit firey between Trump and `${nonWesternWorldLeader}` back then, and so while "the *symptoms* of a *cold* war" may lend itself well to a myxomatosis analogy, I am not convinced that this song and its video aren't discrete.

![Wide Trump](/archive/_images/wide-trump.jpg)<br>
<small>*\[Song for Denise intensifies\]*</small>

But hey, look at me trying to find meaning in Pendulum tracks. Once a fanboy, always a fanboy.

By the way, for those of you wondering: the cover art says "every lover is a soldier; the quarrels of lovers are the renewal of love". Better translations probably exist. I didn't go to Eton *or* own a donkey sanctuary, if you catch my drift.

## Until next time

You've just read an entire article on two singles by some old drum & bass act. Go take a shower. 🄳
