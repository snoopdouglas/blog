# 2020-04-19 - Misadventures in 2.5D

For the longest time I have threatened to start writing a blog - and while I may be a sadist, I am nothing if not fair. This is only happening because the circumstances have become so extreme that their documentation has become an inevitability.

I am, of course, talking about the development of my game. Recently, it hit a roadblock and slowed to a crawl - and, for perhaps the first time in its God-knows-how-long-in-real-terms development process, I have found myself feeling *creatively stifled*.

You're continuing to read this, so I must assume you're also a glutton for punishment. Therefore, you'll forgive me for continuing to explain how I ended up here.

## Kickapoo

A long-ass heckin' time ago in a village called Hoxne, I got into games development and made a very simple space shooter. I later planned to extrapolate its premise (from a vague plot scrawled in the readme) into something with a proper campaign, a proper story, proper levels, and maybe even cutscenes. As it happens, this plan [went a short distance](https://dougthompson.co.uk/engine), but we're not here to talk about that.

![The D](/archive/_images/tenacious-d.jpg)<br>
<small>*It's time for my breakfast, it's time for some cheese.*</small>

Things were pretty different then. C/C++ were generally how one would tackle game logic, and though engines were around, they were generally too heavyweight for my 12-year-old brain, and/or pretty expensive. I needed something more barebones to kick me off, and once I realised a bunch of my PC games were using [Allegro](https://liballeg.org), I went with that.

## Black, Decker & Neckbeard

Allegro was pretty simple to grasp due to its barebones nature. To this day, it maintains this philosophy; one of the first things you'll read on its website is:

> Allegro is *not* a game engine: you are free to design and structure your program as you like.

Complete creative control, right down to the byte. Well, in short, DIY feels great - when you can make it work.

![DIY](/archive/_images/diy.jpg)<br>
<small>*I don't know what these things are, but my dad uses them sometimes.*</small>

For a very long time, I've been operating just within the capacity of the garden shed. I've coded my own sprite packers, level editing tools (of varying quality) - hell, I've even done away with `make` in favour of my own tools that work just *a bit more* like I want them to.

But recently, the first real crack began to appear in the facade - and while it makes sense to me now that this would be the kicker, I didn't realise what I was getting myself into at all.

## 3D

Those of you who've walked this path may well have seen it coming. After a couple of iterations of my swanky (albeit very basic) 3D editor and model formats, I very suddenly just became sick of it all. Things seemed to be progressing well, but something had snapped (in the most boring way possible).

![Asobi Asobase](/archive/_images/asobi-persona.jpg)<br>
<small>*I've never seen this app before! Oh wait, it's one of mine.*</small>

I was more than happy to reinvent the wheel for DIY's sake up until then. Alas, this was where I found myself reaching for [Godot](https://godotengine.org/), an engine that had stood out from the crowd to me whilst I heretically investigated such things. The reasons for this were simple enough:

* Text-based projects. I mean come on, I've gotta *version* these hulking gurt things.
* It's an open system (never mind being MIT licensed).
* A nice-looking editor for both 2D and 3D.

That last point was actually the most divisive in my mind; this is as far from Allegro as I could come, and eventually I thought that if I was  going to make the jump, I should make it big.

---

With that, you're very nearly caught up to the present. Now, onto what I actually wanted to talk about: what I've been up to since I started using it, and how my time with it has been coloured by my *roots*, man.

## Grumpily accepting the future, pt. 1

![Godot](/archive/_images/godot-sadness.png)

Naturally, the first thing I did was download the editor to see whether I could do anything with it uninstructed. This was a predictably bad idea, and after fumbling around in the broad daylight for a bit, I forced myself to watch some slightly-out-of-date YouTube tutorial before accepting that I'd just need to <abbr title="Read The Gosh Darned Manual">RTFM</abbr>.

If the contrast between this and Allegro wasn't clear enough, it wasn't long before the tutorial told me to [imagine myself as a chef](https://docs.godotengine.org/en/3.2/getting_started/step_by_step/scenes_and_nodes.html) in a fully-stocked kitchen. *"Everything you need is right there!"*, I was told as I seethed in rage and begruding agreement - and, after the usual "move a box with the cursor keys" rite of passage and a quick look through the your-first-game app, I decided I'd seen enough to know I had to try a quick reimplementation of my shooter's movement.

## Sorekara do shitano?

It worked absolutely fine and the provided physics engine actually suited it very well.

## Oh okay then

With that success under my belt, I then felt I had to investigate whether my earlier 3D-shaped upset could be remedied by Godot.

At this point, I should probably mention something: the whole reason I was meddling with 3D in the first place was that I'd decided I wanted to make my game *2.5D*. Some [very good shooters](https://en.wikipedia.org/wiki/Radiant_Silvergun) have merged 3D scenery with 2D gameplay, and you'll almost certainly have noted Mario's latest sidescrolling outings have more than a whiff of 3D about them.

![Captain Falcon](/archive/_images/captain-falcon.jpg)<br>
<small>*Pictured: some very special 2.5D nipples.*</small>

So - if at all possible - I wanted to use Godot's 2D physics engine with its 3D renderer. While 3D physics is absolutely something that Godot can do, it was something I wanted to avoid; that Z-axis was left dangling like an appendix.

> **Prior art alert:** I'm aware of a few of Godot's demos that deal with this: [[1]](https://github.com/godotengine/godot-demo-projects/tree/master/misc/2.5d)[[2]](https://github.com/godotengine/godot-demo-projects/tree/master/viewport/2d_in_3d)[[3]](https://github.com/godotengine/godot-demo-projects/tree/master/viewport/3d_in_2d). These don't quite hit the nail on the head, though [1] comes the closest, and it might be an example I look into if I make an isometric platformer.

## Entanglement

As it turns out, putting 3D nodes inside of 2D nodes (and vice versa) is more than possible. For instance, [KinematicBody2D](https://docs.godotengine.org/en/3.2/classes/class_kinematicbody2d.html) - the node-de-choix for moving a 2D player character around - can be given a [MeshInstance](https://docs.godotengine.org/en/3.2/classes/class_meshinstance.html) child, no worries.

However, the problem here is positioning. When 2D nodes have 2D children, their children's position is relative to the parent; you move the parent, you move the children too. It's exactly the same story with 3D nodes and their children, just with an additional axis.

![Highly obscure reference](/archive/_images/ygotas-serenity.png)<br>
<small>*You don't have a ghost of a chance of understanding this reference.*</small>

So, what happens when you mix 2D and 3D? Well, the answer - for better or worse - is a solid *nothing*. Moving a 2D parent won't move its 3D children, and vice versa.

> **Ackchtually:** if it were the case that 2D and 3D nodes could translate each other, this'd hugely complicate things - but we won't go into that here.

So, we have to make one of these nodes update the other's position. Some thinking was required on how best to do this - and hopefully, I came out with a good answer.

## Dom/Sub

If you're anything like me, you like to try new things. Right? Anyway, back to games programming.

![IDE](/archive/_images/ide.jpg)<br>
<small>*Pick one. You can't be both.*</small>

At this point, it was obvious that the 2D node had to wear the trousers in terms of positioning - because it's the one that interacts with the 2D physics engine.

Despite this, I found that the whole thing worked best with Godot's editor when I set the 3D node to be the *parent* of the 2D node; then, I employed some relatively fancy scripting to make everything work as expected. The main concern here was making *visual editing* possible; I want to be able to move stuff around in the editor (in 3D), but when I *run* the game, I want the 2D physics engine to take over.

Tad complex, no? Well, here's what I ended up with:

* In both 2D and 3D space, we centre our playfield on the origin.
  * This isn't the default for 2D. Normally, `(0,0)` is the top-left of the screen.
* In 3D, we use an orthographic camera positioned at `(0,100,0)` which looks at the origin, angled with negative Z as 'up'.
* When a 2.5D object enters the scene, its 2D X and Y are assigned from its 3D X and Z.
  * Yes, this is starting to get a bit weird. Hang tight.
* Generally, after an object's 2D physics are processed, it calls a `to_3d()` function which updates its 3D X and Z from its 2D X and Y.

![Ship nodes](/archive/_images/25d-ship-nodes.png)<br>
<small>*The first actually useful image in this blog: here's the player's 2.5D ship in Godot. **ShipPrototype** is the 3D model. The **KinematicBody2D** interacts with the physics engine, providing updated transformations to the root node, **Ship**. Incidentally, the root node is a new type that implements the 2D-3D kinkiness described above.*</small>


## Royale with cheese

This seems like a fair bit of pokery - but, believe it or not, I did have some rationale for this. The goal was to make working with this whole 2.5D thing as intuitive as possible. So, let's address the obvious questions:

* Why centre the 2D origin?
* Why put the camera up high?
* Why swap the Y and Z axes when going from 2D to 3D?

The short answer to all of these questions is that in 3D, [Godot uses the metric coordinate system](https://docs.godotengine.org/en/3.2/tutorials/3d/introduction_to_3d.html#coordinate-system). In said system, the positive Y axis is 'up'. This is contrary to the 2D coordinate system, where positive Y is 'down'.

![Samuel L Jackson](/archive/_images/samuel-l-jackson-france.jpg)<br>
<small>*It was either this or a picture of him wearing a beret.*</small>

As per usual for 3D, though, the origin is the centre of the world. All 3D tooling I've used - Godot included - has the origin as the centre of *anything*, be it the world, the screen, or 3D models - so the first decision taken was to make our 2D world play by these rules too. Though I'm used to the 2D origin being in the top-left, this wasn't too jarring a change to make in my mind.

Now, let's say we move the camera to somewhere high on the Y-axis, looking at the origin. This means we're now looking down at the X and Z axes, and - you might have guessed it - the positive Z axis now looks like it's 'down'. This means we get another easy win for 2D-3D coordinate system parity, albeit with the annoyance of having to swap the axes in that `to_3d()` function I mentioned.

Another sneaky convenience of this is that the idea of this being a 'top-down' shooter is kinda confirmed by our camera positioning. It's at the top, looking down on the action. *Insert smirk here.*

![View modes](/archive/_images/25d-view-modes.png)

<small>*Here's a comparison of Godot's 3D and 2D editor modes (respectively), with everything I just described in place.*</small>

<small>*In the 3D view, we're using the **Top Orthogonal** view, looking down the Y-axis; you can see the player's ship, albeit obscured by the camera gizmo, which is above it. The grey bars at the top and bottom are nodes I added to help me visualise the player viewport.*</small>

<small>*In the 2D view, nothing is drawn except the camera outline and the ship's hitbox.*</small>


## Urgh, anything else?

There are a couple of other things to mention, yes.

* The camera's size is relatively large, so as to give pixel-precision. Why? Well: I wanted a low-poly, low-res look for the game. The viewport is locked at **384x216**, and scaled up *with no filtering* to fill the screen.

  You'll remember that we centred our 2D world on the origin, meaning that we have a visible playfield from `(-192,-108)` to `(192,108)`. The 3D camera implements this, and thus its size has to be `384`.

  ![Perspective camera](/archive/_images/25d-perspective.png)<br>
  <small>*The 3D world from a perspective camera. Note the big-ass pink camera viewbox.*</small>

  The Godot editor's 3D grid is currently better designed for smaller-scale stuff, if I'm being honest. Simple parity between 2D and 3D coordinates was just more important to me though, so I resized the grid to 384x384 and increased the draw distance. It seems to work alright, even on my 2017 i7 laptop.

* All of this 2.5D functionality ended up being shoved in a reusable class, given that I've got plenty of in-game objects that'll be using it. On the subject, our `to_3d()` function isn't called automatically after every physics cycle, because I want other ways to move these objects too (eg. in cutscenes).

## Bye

Yeah, you've sat through enough. And now the bad news: there might be more of these. Laters. 🄳
